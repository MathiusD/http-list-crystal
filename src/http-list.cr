require "kemal"

ALL_DATA = Hash(String, Array(String)).new

before_all do |env|
  env.response.headers["Access-Control-Allow-Origin"] = "*"
  env.response.headers["Access-Control-Allow-Methods"] = "*"
  env.response.headers["Access-Control-Allow-Headers"] = "*"
end

options "/*" do |env|
end

post "/:name/reset" do |env|
  name = env.params.url["name"].as(String)
  if ALL_DATA.has_key?(name)
    ALL_DATA.reject!(name)
  else
    env.response.respond_with_status 404, "Empty data with following name : #{name}"
  end
end

put "/:name/push" do |env|
  name = env.params.url["name"].as(String)
  data = env.params.query["data"].as(String)
  if ALL_DATA.has_key?(name)
    ALL_DATA[name] << data
  else
    ALL_DATA[name] = [data]
  end
end

get "/:name/pop" do |env|
  name = env.params.url["name"].as(String)
  if ALL_DATA.has_key?(name) && ALL_DATA[name].size > 0
    env.response.content_type = "text/plain"
    "#{ALL_DATA[name].pop}"
  else
    env.response.respond_with_status 404, "Empty data with following name : #{name}"
  end
end

get "/:name/shift" do |env|
  name = env.params.url["name"].as(String)
  if ALL_DATA.has_key?(name) && ALL_DATA[name].size > 0
    env.response.content_type = "text/plain"
    "#{ALL_DATA[name].shift}"
  else
    env.response.respond_with_status 404, "Empty data with following name : #{name}"
  end
end

get "/:name/show" do |env|
  name = env.params.url["name"].as(String)
  if ALL_DATA.has_key?(name)
    env.response.content_type = "application/json"
    ALL_DATA[name].to_json
  else
    env.response.respond_with_status 404, "Empty data with following name : #{name}"
  end
end

Kemal.run
